/**
 * This object holds the definition(the default values) for PPM SYNC STORAGE DATA
 * that will keep all user settings for the profile
 * all data here will be encrypted(by selected ES) before writting it out
 *
 * @type {Object}
 */
function PPM_TYPE_STORAGEDATA_SYNC() {
    var inited = false;
    var default_profile_name = "PPMCONFIG";
    var default_master_key = "Paranoia";
    var default_enryption_scheme = "PPM_ENCTYPTION_SCHEME_AESMD5";
    var storage_profile_name;
    var storage_profile_master_key;
    var storage_profile_enryption_scheme;

    var data = {
        logincount: 0,
        logindate: ""
    };

    /*-------------------------------------------------------------------------------------PUBLIC METHODS*/
    this.getOption = function(key) {
        return(_getOption(key));
    }

    this.setOption = function(key, val, callback) {
        _setOption(key, val, callback);
    }

    this.init = function(pn, mk, es, callback) {
        var settingsObject = null;
        pn = (pn?pn:default_profile_name);
        chrome.storage.sync.get(null, function(d){
            if(!d[pn]) {
                console.log("STORAGE(SYNC): PROFILE DOES NOT EXIST [KEY="+pn+"]!");
                if(pn!=default_profile_name) {
                    return;//bail out - maybe call callback with false??
                } else {
                    console.log("STORAGE(SYNC): CREATING DEFAULT DATA[KEY="+pn+"]...");
                    settingsObject = {};
                    storage_profile_name = default_profile_name;
                    storage_profile_master_key = default_master_key;
                    storage_profile_enryption_scheme = default_enryption_scheme;
                }
            } else {
                var cryptedSettings = d[pn];
                if(mk&&es) {
                    console.log("STORAGE(SYNC): RAW DATA[KEY="+pn+"]: " + JSON.stringify(cryptedSettings));
                    var CRYPTER = PPM.getCrypter();
                    var decryptedSettings = CRYPTER.decryptWithScheme(cryptedSettings, mk, es);
                    console.log("STORAGE(SYNC): DECRYPTED DATA: " + decryptedSettings);
                    try {//if the parsed decrypted settings result an object then we are OK
                        var so = JSON.parse(decryptedSettings);
                        if (typeof(so) == "object") {
                            storage_profile_name = pn;
                            storage_profile_master_key = mk;
                            storage_profile_enryption_scheme = es;
                            settingsObject = so;
                            console.log("STORAGE(SYNC): Loaded.");
                        } else {
                            throw("Arrrggghh!");
                        }
                    } catch (e) {
                        console.log("STORAGE(SYNC):This MasterKey does not open the door!");
                    }
                } else {
                    console.log("STORAGE(SYNC): No MK and/or ES were given to decrypt data[KEY="+pn+"]!");
                }
            }
            //
            if(settingsObject) {
                var _needsWrite = false;
                if (!_checkForAllDataKeys(settingsObject)) {
                    settingsObject = _injectMissingDataKeys(settingsObject);
                    _needsWrite = true;
                    console.log("STORAGE(SYNC): Added missing props to loaded data: " + JSON.stringify(settingsObject));
                }
                data = settingsObject;
                if(_needsWrite) {
                    _writeOutStorageData(null);
                }
                inited = true;
            } else {
                console.log("STORAGE(SYNC): No profile data was loaded!");
            }
        });
    }

    this.shutdown = function(callback) {
        //we need flag to be able to track if changes were made since last setOption
        _writeOutStorageData(callback);
    }

    this.isInited = function() {return(inited);}
    /*-------------------------------------------------------------------------------------PRIVATE METHODS*/
    function _getOption(key) {
        var answer = null;
        if(data[key]) {
            answer = data[key];
        }
        return(answer);
    }

    function _setOption(key, val, callback) {
        data[key] = val;
        console.log("STORAGE(SYNC): SET("+key+"):"+val);
        //we assume that if you set callback you want writeOut (which may not at all be the case)
        if(callback) {
            _writeOutStorageData(callback);
        }
    }

    function _writeOutStorageData(callback) {
        var pdstr = JSON.stringify(data);
        console.log("STORAGE(SYNC): WRITING OUT:"+pdstr);
        var CRYPTER = PPM.getCrypter();
        var pdencstr = CRYPTER.encryptWithScheme(pdstr, storage_profile_master_key, storage_profile_enryption_scheme);
        console.log("STORAGE(SYNC): WRITING OUT:"+pdencstr);
        var o = {};
        o[storage_profile_name] = pdencstr;
        chrome.storage.sync.set(o, function() {
             console.log("STORAGE(SYNC): SAVED PROFILE("+storage_profile_name+").");
             if(callback) {callback();};
        });
    }

    function _injectMissingDataKeys(d) {
        for(var prop in data) {
            if (!d.hasOwnProperty(prop)) {
                d[prop] = data[prop];
            }
        }
        return(d);
    }

    function _checkForAllDataKeys(d) {
        var answer = true;//optimistic
        for(var prop in data) {
            if (!d.hasOwnProperty(prop)) {
                answer = false;
                break;
            }
        }
        return(answer);
    }
};

