/**
 * This object holds the definition(the default values) for PPM LOCAL STORAGE DATA
 * mostly interface preferences
 * !!!NO ENCRYPTION!!!
 *
 * @type {Object}
 */
function PPM_TYPE_STORAGEDATA_LOCAL() {
    var inited = false;
    var data = {
        "my_data": 1,
        "your.data": "Jack"
    };//set some default values for first timers

    /*-------------------------------------------------------------------------------------PUBLIC METHODS*/
    this.getOption = function(key) {
        return(_getOption(key));
    }

    this.setOption = function(key, val, callback) {
        _setOption(key, val, callback);
    }



    this.init = function() {
        chrome.storage.local.get(null, function(d){
            console.log("STORAGE(LOCAL): Loaded:"+d+" :: " + JSON.stringify(d));
            if (!_checkForAllDataKeys(d)) {
                d = _injectMissingDataKeys(d);
                console.log("STORAGE(LOCAL): Added missing props to loaded data: " + JSON.stringify(d));
            }
            inited = true;
        });
    }

    this.shutdown = function() {
        //dummy function - there is nothing to do here because after
        //shutdown a new instance of this will be created
    }

    this.isInited = function() {return(inited);}
    /*-------------------------------------------------------------------------------------PRIVATE METHODS*/
    function _getOption(key) {
        var answer = null;
        if(data[key]) {
            answer = data[key];
        }
        return(answer);
    }

    function _setOption(key, val, callback) {
        var o = {};
        o[key] = val;
        chrome.storage.local.set(o, function() {
            data[key] = val;
            console.log("STORAGE(LOCAL): SAVED("+key+"):"+val);
            if(callback) {callback();};
        });
    }

    function _injectMissingDataKeys(d) {
        for(var prop in data) {
            if (!d.hasOwnProperty(prop)) {
                _setOption(prop, data[prop], null);
            }
        }
        return(d);
    }

    function _checkForAllDataKeys(d) {
        var answer = true;//optimistic
        for(var prop in data) {
            if (!d.hasOwnProperty(prop)) {
                answer = false;
                break;
            }
        }
        return(answer);
    }
};