$("document").ready(function() {
    console.log("[POPUP]: popup-init....");

    checkInitPanels();
    $("#panel_menu").accordion();
});

function checkInitPanels() {
    var STORAGE = PPM.getStorage();
    if(STORAGE.isInited()) {
        initMenuPanel();
    } else {
        initLoginPanel();
    }
}

/*-------------------------------------------------------------------------------------------------------LOGIN*/
function initLoginPanel() {
	$("#panel_menu").hide();
	$("#panel_login").show();
    //
    $("#panel_login input#masterkey").val("Paranoia");
    //
	$("input#button_login").bind("click.ppm", doLogin);
}

function doLogin() {
    var pn = $("#panel_login select#profilename").val();//PROFILE TO USE
	var mk = $("#panel_login input#masterkey").val();
	var es = $("#panel_login select#encryptionscheme").val();
    console.log("[POPUP]: Logging in..."+pn);
    var STORAGE = PPM.getStorage();
	STORAGE.init(pn, mk, es, doLoginDone);
	//wait...
}
function doLoginDone() {
	console.log("logged in");
    $("input#button_login").unbind(".ppm");
    checkInitPanels();
    window.close();
}

/*-------------------------------------------------------------------------------------------------------MENU*/
function initMenuPanel() {
	$("#panel_login").hide();
	$("#panel_menu").show();
	$("li#menu_item_logout").bind("click.ppm", doLogout);
}

function doLogout() {
    //if(confirm("Are you sure you want to logout?")) {
        var STORAGE = PPM.getStorage();
        STORAGE.shutdown(doLogoutDone);
    //}
}

function doLogoutDone() {
	console.log("logged out");
    $("li#menu_item_logout").unbind(".ppm");
    checkInitPanels();

}