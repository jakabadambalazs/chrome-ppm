function ParanoiaPasswordManager() {
    var CRYPTER;
    var STORAGE;

    /*-------------------------------------------------------------------------------------PUBLIC METHODS*/
    this.getCrypter=function(){return(CRYPTER);}
    this.getStorage=function(){return(STORAGE);}




    //MAIN BACKGROUND SCRIPT INIT SEQUENCE
    this.init = function() {
        console.log("---PPM---phase 0...");
        CRYPTER = new PPM_CRYPTER();//bg_crypter.js
        CRYPTER.init(PPM_init_phase_1);
    }

    function PPM_init_phase_1() {
        console.log("---PPM---phase 1...");
        STORAGE = new PPM_STORAGE();//bg_storage.js
        STORAGE.init(null, null, null, PPM_init_phase_2);
    }

    function PPM_init_phase_2() {
        console.log("---PPM---phase 2...");
        //chrome.browserAction.setBadgeText({"text": getPPMOption("general.logincount", "sync").toString()});
    }






}


//Run
var PPM;
document.addEventListener('DOMContentLoaded', function () {
    PPM = new ParanoiaPasswordManager();
    PPM.init();
});




//OMNIBOX
chrome.omnibox.onInputEntered.addListener(function(cmd) {
    switch(cmd){
        case "killallstorage":
            if(confirm("Are you sure to kill all local and sync storage?")) {
                chrome.storage.local.clear();
                chrome.storage.sync.clear();
                alert("All storage areas have been cleared!");
                PPM.init();
            }
            break;
        default:
            alert('PPM - I do not understand the command: "' + cmd + '"');
    }

});

