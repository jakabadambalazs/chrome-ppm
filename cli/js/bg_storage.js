/**
 *
 * @constructor
 */
function PPM_STORAGE() {
    var storage_data_local;
    var storage_data_sync;

    /*-------------------------------------------------------------------------------------PUBLIC METHODS*/
    this.init = function(pn, mk, es, callback) {
        console.log("[STORAGE]: Initing...");
        chrome.browserAction.setIcon({"path":"images/paranoia_19_off.png"});
        storage_data_local = null;
        storage_data_sync = null;
        //
        if (!this.isInited()) {
            storage_data_local = new PPM_TYPE_STORAGEDATA_LOCAL();
            storage_data_local.init();
            storage_data_sync = new PPM_TYPE_STORAGEDATA_SYNC();
            storage_data_sync.init(pn, mk, es, callback);//"PPMCONFIG", "ParanoiaX", "PPM_ENCTYPTION_SCHEME_AESMD5"
            //
            var self = this;
            var num_tries = 1;
            var max_tries = 20;
            var tmr = setInterval(function(){
                if(self.isInited()) {
                    clearInterval(tmr);
                    chrome.browserAction.setIcon({"path":"images/paranoia_19.png"});
                    storage_data_sync.setOption("logincount", Number(storage_data_sync.getOption("logincount"))+1);

                    if(callback) {callback();};
                } else  {
                    //console.log("waiting["+num_tries+"/"+max_tries+"]...");
                    num_tries++;
                    if(num_tries>=max_tries){
                        clearInterval(tmr);
                        console.log("[STORAGE]: Given up...");
                    }
                }
            }, 250);
        } else {
            if(callback) {callback();};
        }
    }

    this.shutdown = function(callback) {
        console.log("[STORAGE]: Shutting down...");
        storage_data_local.shutdown();
        storage_data_sync.shutdown(callback);
        storage_data_local = null;
        storage_data_sync = null;
        chrome.browserAction.setIcon({"path":"images/paranoia_19_off.png"});
    }

    this.isInited = function() {
        return(storage_data_local && storage_data_local.isInited() && storage_data_sync && storage_data_sync.isInited());
    }

}