

function fillOptionsFields() {	
	$("input#testopt").val(Number(_BG_.getPPMOption("config.testopt", "sync")));
}

function saveAction() {
	_BG_.setPPMOption("config.testopt", $("input#testopt").val(), "sync");
}

function resetAction() {
	fillOptionsFields();
}



function initTab(tabIndex) {
	switch(tabIndex) {
		case 0:
			$("#msg").html("Passcards tab ready.");
			break;
		case 1:
			$("#msg").html("Servers tab ready.");
			break;
		case 2:
			$("#msg").html("Options tab ready.");
			fillOptionsFields();
			break;
		default:
			break;
	}
}


$(function() {
	//TODO: Init should be blocked until storagedata is NOT available
	
	
	//init TABS
	var tabIndex = _BG_.getPPMOption("options_tab_index", "local");
	$( "#content" ).tabs({
		active: tabIndex,
		activate: function( event, ui ) {
			var tabIndex = Number($(this).tabs("option","active"));
			console.log("active tab: " + tabIndex);
			_BG_.setPPMOption("options_tab_index", tabIndex, "local");
			initTab(tabIndex);
		}
	});
	
	$("input#button_options_save").click(saveAction);
	$("input#button_options_reset").click(resetAction);
	
	//
	initTab(tabIndex);
	
});